# Contact App
- The app should get a contact list from the device and display a list of all contacts in the app.
- It is possible to tap on a particular contact and, by tapping on it, the phone app will be opened with a pre-filled phone number of the contact.
- The app should implement two types of lists using a ListView and a RecyclerView. To switch between them, add the navigation drawer to the app with two options, a ListView and a RecyclerView.
- Each implementation for displaying the contact list should be in a separate fragment.
- Each contact item in the list should contain a contact name, a phone number, and an image. In case there is no image, show a default drawable that can be created using Android Studio.
