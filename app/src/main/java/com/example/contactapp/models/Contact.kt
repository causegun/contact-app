package com.example.contactapp.models

import android.graphics.Bitmap

data class Contact(val name: String?, val phoneNumber: String?, val photoBitmap: Bitmap?)

