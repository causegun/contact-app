package com.example.contactapp.adapter

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.contactapp.R
import com.example.contactapp.models.Contact

class ContactRecyclerViewAdapter(private val contacts: List<Contact>) :
    RecyclerView.Adapter<ContactRecyclerViewAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTextView: TextView = itemView.findViewById(R.id.name_text_view)
        val phoneTextView: TextView = itemView.findViewById(R.id.phone_text_view)
        val photoImageView: ImageView = itemView.findViewById(R.id.photo_image_view)

        init {
            itemView.setOnClickListener {
                val position = adapterPosition
                val intent =
                    Intent(Intent.ACTION_DIAL, Uri.parse("tel:${contacts[position].phoneNumber}"))
                itemView.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.contact_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = contacts[position]

        holder.nameTextView.text = contact.name
        holder.phoneTextView.text = contact.phoneNumber
        if (contact.photoBitmap != null) {
            holder.photoImageView.setImageBitmap(contact.photoBitmap)
        } else {
            holder.photoImageView.setImageResource(R.drawable.default_contact_image)
        }
    }

    override fun getItemCount(): Int {
        return contacts.size
    }
}
