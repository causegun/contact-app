package com.example.contactapp.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.contactapp.R
import com.example.contactapp.models.Contact

class ContactListViewAdapter(context: Context, contacts: List<Contact>) :
    ArrayAdapter<Contact>(context, 0, contacts) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var itemView = convertView
        if (itemView == null) {
            itemView = LayoutInflater.from(context).inflate(R.layout.contact_item, parent, false)
        }

        val contact = getItem(position)

        val nameTextView = itemView!!.findViewById<TextView>(R.id.name_text_view)
        val phoneTextView = itemView.findViewById<TextView>(R.id.phone_text_view)
        val photoImageView = itemView.findViewById<ImageView>(R.id.photo_image_view)

        nameTextView.text = contact?.name
        phoneTextView.text = contact?.phoneNumber

        if (contact?.photoBitmap != null) {
            photoImageView.setImageBitmap(contact.photoBitmap)
        } else {
            photoImageView.setImageResource(R.drawable.default_contact_image)
        }

        itemView.setOnClickListener {
            val phoneUri = Uri.parse("tel:${contact?.phoneNumber}")
            val dialIntent = Intent(Intent.ACTION_DIAL, phoneUri)
            context.startActivity(dialIntent)
        }

        return itemView
    }
}