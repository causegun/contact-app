package com.example.contactapp.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentUris
import android.graphics.BitmapFactory
import android.provider.ContactsContract
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import com.example.contactapp.models.Contact

abstract class BaseFragment : Fragment() {

    fun requestReadContactsPermission() {
        val permissionLauncher =
            registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
                if (isGranted) {
                    setAdapter()
                } else {
                    Toast.makeText(requireContext(), "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        permissionLauncher.launch(Manifest.permission.READ_CONTACTS)
    }

    @SuppressLint("Range")
    fun loadContacts(): MutableList<Contact> {
        val cursor = requireContext().contentResolver.query(
            ContactsContract.Contacts.CONTENT_URI,
            null, null, null, null
        )

        val contacts = mutableListOf<Contact>()

        cursor?.let {
            while (it.moveToNext()) {
                val name = it.getString(it.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                val id = it.getString(it.getColumnIndex(ContactsContract.Contacts._ID))

                val photoUri = ContentUris.withAppendedId(
                    ContactsContract.Contacts.CONTENT_URI,
                    id.toLong()
                )
                val photoStream = ContactsContract.Contacts.openContactPhotoInputStream(
                    requireContext().contentResolver, photoUri
                )
                val photo = if (photoStream != null) {
                    BitmapFactory.decodeStream(photoStream)
                } else {
                    null
                }

                val phoneCursor = requireContext().contentResolver.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    "${ContactsContract.CommonDataKinds.Phone.CONTACT_ID} = ?",
                    arrayOf(id),
                    null
                )

                val phoneNumber = if (phoneCursor != null && phoneCursor.moveToFirst()) {
                    phoneCursor.getString(
                        phoneCursor.getColumnIndex(
                            ContactsContract.CommonDataKinds.Phone.NUMBER
                        )
                    )
                } else {
                    null
                }

                phoneCursor?.close()

                val contact = Contact(name, phoneNumber, photo)
                contacts.add(contact)
            }

            it.close()
        }

        return contacts
    }

    abstract fun setAdapter()
}