package com.example.contactapp.fragment

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.contactapp.adapter.ContactRecyclerViewAdapter
import com.example.contactapp.R

class RecyclerViewFragment : BaseFragment() {
    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_recyclerview, container, false)
        recyclerView = view.findViewById(R.id.recyclerview_contacts)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        if (ContextCompat.checkSelfPermission(requireContext(),
                Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED) {
            requestReadContactsPermission()
        } else {
            setAdapter()
        }

        return view
    }

    override fun setAdapter() {
        val adapter = ContactRecyclerViewAdapter(loadContacts())
        recyclerView.adapter = adapter
    }
}
