package com.example.contactapp.fragment

import android.Manifest.permission.READ_CONTACTS
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.core.content.ContextCompat
import com.example.contactapp.adapter.ContactListViewAdapter
import com.example.contactapp.R

class ListViewFragment : BaseFragment() {
    private lateinit var listView: ListView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_listview, container, false)
        listView = view.findViewById(R.id.listview_contacts)
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestReadContactsPermission()
        } else {
            setAdapter()
        }

        return view
    }

    override fun setAdapter() {
        val adapter = ContactListViewAdapter(requireContext(), loadContacts())
        listView.adapter = adapter
    }

}

